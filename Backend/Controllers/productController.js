import asyncHandler from 'express-async-handler'
import Product from '../models/productModel.js'


//@desc Fetach all Products 
//@route GET /api/products
//@access public
const getproducts = asyncHandler(async (req, res) => {
    const products = await Product.find({})
    res.json(products)
})


//@desc Fetach single Product
//@route GET /api/products/:id
//@access public

const getproductById = asyncHandler(async (req, res) => {
    const product = await Product.findById(req.params.id)

    if (product) {
        res.json(product)
    } else {
        res.status(404)
        throw new Error('Product not found')
    }

})

export { getproducts, getproductById }
