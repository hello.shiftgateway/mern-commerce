import express from 'express'
const router = express.Router()
import { getproductById, getproducts } from '../Controllers/productController.js'


router.route('/').get(getproducts)
router.route('/:id').get(getproductById)


export default router